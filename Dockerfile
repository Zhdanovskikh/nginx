FROM nginx
FROM busybox

CMD /bin/sh -c chmod g+rwx /var/cache/nginx /var/run /var/log/nginx

EXPOSE 80
